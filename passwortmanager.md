# Der eigene Passwort-Manager

## Was sind Kriterien 
Speicherort Land
Wie es gespeichert wird/Verschlüsselung
Backupmöglichkeiten

## Analyse und Vergleich


|                 | **Keepass**         | Wertung | **Lastpass** | Wertung       | **1Password** | Wertung | **Bitwarden**| Wertung|
|-----------------|---------------------|---------|--------------|---------------|---------------|---------|--------------|--------|
| Link            | [keepass.info](https://keepass.info/)   |  | [lastpass.com](https://www.lastpass.com/de)  |  |[1password.com](https://1password.com/)  |   |[bitwarden.com](https://bitwarden.com/)    |   |
| Verschlüsselung | AES-256, ChaCha20 | ✓ erfüllt | AES-256  | ✓ erfüllt       | AES-256 | ✓ erfüllt | AES-256 | ✓ erfüllt |
| OpenSource      | Ja                | ✓ erfüllt  | nein     | ╳ nicht erfüllt | nein | ╳ nicht erfüllt | ja | ✓ erfüllt | 
| Ablageort/Datenbank| Lokal | ✓ erfüllt | In der Cloud | ╳ nicht erfüllt | In der Cloud | ╳ nicht erfüllt | Cloud oder Self Hosted | ✓ erfüllt |
| Verfügbarkeit | MacOS, Linux, Windows| teilweise erfüllt | Windows, MacOS, Android, iOS | ✓ erfüllt | Windows, MacOs, Linux, Android, iOS | ✓ erfüllt | Windows, MacOs, Linux, Android, iOS | ✓ erfüllt |

Anhand dieser Analyse het sich herausgestellt, dass sich Bitwarden am besten für mich geeignet ist. Wir sind im Unternehmen, in dem ich angestellt bin im wechsel, von 1Password auf Bitwarden. Deshalb habe ich die Bitwarden Clients schon auf all meinen Geräten eingerichtet.

## Passwortmanager einrichten

### Was tun Sie, wenn Sie ihr Master-Passwort vergessen?
Einen neuen Account festlegen. Da Bitwarden selbst das Master-Passwort nicht weiss, gibt es keine möglichkeit dieses zurück zu setzen. Ich kann, wenn ich einen Account, den ich als Notfallkontakt registriert habe, auf die Passwörter zugreiffen, und diese in meinen neuen Account übernehmen.

### Wie erstellen Sie ein Backup von Ihrer Passwortdatenbank?
Da wir Bitwarden selber hosten, wird bei uns der ./bwdata Ordner auf einer unserer Backupserver überspielt. So können wir im Notfall alle Daten wiederherstellen.