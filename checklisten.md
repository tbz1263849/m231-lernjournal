# Checklisten des Datenschutzbeauftragten des Kanotn Zürichs

# Checkliste Smartphone-Sicherheit in 10 Schritten erhöhen

Link: [datenschutz.ch](https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen)

1. Aktiviern Sie die Gerätesperre
   - Ja ich habe die Automatische Gerätesperre aktiviert und mit Gesichtserkennung und Pin gesichert

2. Sperren und löschen Sie das Gerät bei Verlust sofort.
   - Auf einem anderen Apple Gerät, welches die Find My App installiert hat oder unter [icloud.com](icloud.com) kann man Appelgeräte sperren und auch löschen.

3. Installieren Sie Apps nur aus vertrauenswürdigen Quellen.
   - Bei iPhones können Apps nur aus dem App Store geladen werden. Diese Apps werden von Apple geprüft, dass sie keine schädlichen Software beinhalten.

4. Nutzen Sie öffentliches WLAN mit Vorsicht.
   - Wenn immer möglich nutze ich nur das Mobilnetz.

5. Installieren Sie updates.
   - Wenn immer ein Update erhältlich ist, installiert dies mein iPhone automatisch in der Nacht.

6. Löschen Sie Daten sicher.
   - Wenn ein iPhone nicht richtig zurückgesetzt worden ist, kann es nur mit einem Passwort und 2-Faktor-Authentifizierung wiederverwendet werden.

7. Beachten Sie allgemeine Vorsichtsegeln.
   - Verdächtige Mails / Nachrichte öffne ich nicht
   - Keine verdächtigen Anhänge und Links öffnen
   - Ich gebe bei unbekannten keine Persönliche Daten preis.

8. Deaktivieren Sie drahtlose Schnittstellen
   - Ich erlaube nur Apps welche wirklich Ortungsdienste benötigen, diese zu verwenden.
   - NFC ist bei iPhones nur sehr eingeschränkt benutzbar. Z.B. nur mit anderen Apple Geräten oder mit von Apple freigegebenen Geräten verwendbar und kann auch nicht deaktiviert werden.

9. Schränken Sie die Synchronisierung ein. 
   - Dies mache ich nicht, da ich aktiv die Cloud als Speicherort verwende. Ich habe die Risiken abgeschätzt und mir sind die Konsequenzen bekannt.

10. Benutzen Sie fürs Login einen zweiten Faktorn
   - Wo immer möglich aktiviere ich die Zwei-Faktor-Authentifizierung

# PC-Sicherheit erhöhen - in fünf Schritten

[datenschutz.ch](https://www.datenschutz.ch/meine-daten-schuetzen/pc-sicherheit-erhoehen-in-fuenf-schritten)

1. Persönliche Informationen schützen
   - Ich benutze den Brave Browser dieser hat diverse Security Features standartmässig eingeschaltet. Diese sind zum beispiel ein Tracker- und Werbeblockersystem.

2. Angriffe abwehren
   - Sicherheitsrelevante Updates werden bei mir in der regel automatisch Heruntergeladen
   - Meine Firewall ist aktiviert
   - MacOS ist von standart aus sehr restriktiv z.B. werden Apple nicht bekannte Programme erst nach manueller Bestätigung installiert.

3. Zugriffe Unberechtigter verhindern
   - Alle meine Passwörter sind durch einen Passwort-Generator zufällig generiert
   - Ich nutze grundsätzlich keine öffentlichen Netzwerke, lieber mein Handy-Hotspot
   - Alle meine Privaten Netze sind verschlüsselt
   - Mein Laptop sperrt sich automatisch nach einer Minute Inaktivität. Grundsätzlich sperre ich ihn aber immer sobald ich meinen Arbeitsplatz verlasse.

4. Verschlüsseln Sie sensitive Inhalte
   - All meine Sensitiven wie auch die meisten nicht sensitiven Daten sind verschlüsselt.

5. Sichern Sie Informationen und löschen Sie Daten vollständig
   - Mein Homeserver macht täglich ein Backup von meinem MacBook sobald der Mac sich mit meinem WLAN zuhause verbindet
   - meine Daten sind Diebstahlsicher auf meinem Server zuhause gespeichert, wenn allerdings mein Haus niederbrennt habe ich ehrlich gesagt andere Sorgen, als die Ferienfotos von vor 3 Jahren.
