# M231-Lernjournal

Dies ist eine Git Ablage für das Modul M231 an der TBZ.

## Inhalt

Wir haben im Modul m231 gelernt, wie man Datenschutz und Datensicherheit anwendet. Wir haben angeschaut, wie man elektronischen Geräten wie Smartphone oder Laptop sicher einrichtet, sichere Passwörter erstellt und wo man Sie speichert uvm.

## Inhaltsverzeichnis

- [1. Einführung Markdown](markdown.md)
- [2. Checklisten dsb](checklisten.md)
- [3. Evaluierung Passwortmanager](passwortmanager.md)

Author Luca Suter
